dirhead

list files in a directory that may be ridiculously large

the original version was written live on a Solaris production box to help list
a netapp-mounted directory containing so many files that no more could be
written (and using ls was taking tens of minutes). I used it to step through
all the files a chunk at a time and sort them into subdirectories, then I
lost that version.

later I decided to try recreating it and this is the result. No doubt there
is some better way to do literally everything in here (patches welcome)

except for the goto, I am keeping that for fun
