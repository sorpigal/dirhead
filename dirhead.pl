#!/usr/bin/perl
use strict;
use warnings;
use v5.12;

my $dir = shift || '.';
my $n = shift || 10;
my $offset = shift || 0;
my $c = 0;

opendir(my $dh, $dir) || die $!;
while (readdir $dh) {
	next unless $c++ > $offset;
	next if $_ eq '.' || $_ eq '..';
	say $_;
	--$n > 0 || last;
}
closedir($dh) or die $!;

