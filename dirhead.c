#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <getopt.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <libintl.h>
#include <locale.h>
#include <string.h>
#include "dirhead.h"

int main(int argc, char** argv) {
	dirhead_options* options;

	options = mkopts(argc, argv);

	if (optind < argc) {
		for (;optind < argc; optind++) {
			if (bad_arg(argv[optind])) {
				continue;
			}
			dir_head(argv[optind], options);
		}
	} else {
		fprintf(stderr,
			"%s: no director{y,ies} specified\n\n",
			APPNAME
		);
		usage(stderr);
	}

	free(options);

	return EXIT_SUCCESS;
}

int bad_arg(char* arg) {
	struct stat info;

	if (stat(arg, &info) != -1) {
		if (!S_ISDIR(info.st_mode)) {
			fprintf(stderr, "%s: Not a directory\n", arg);
			return 1;
		}
	} else {
		if (ENOENT == errno) {
			perror(arg);
		}
		if (ENOTDIR == errno) {
			perror(arg);
		}
		return 1;
	}

	return 0;
}

dirhead_options* mkopts(int argc, char** argv) {
	char* shortops = "ho:n:Vt:d:0:H";
	struct option longops[] = {
		{ "help", no_argument, NULL, 'h' },
		{ "offset", required_argument, NULL, 'o' },
		{ "entries", required_argument, NULL, 'n' },
		{ "version", no_argument, NULL, 'V' },
		{ "type", required_argument, NULL, 't' },
		{ "delimiter", required_argument, NULL, 'd' },
		{ "null-terminate", no_argument, NULL, '0' },
		{ "no-hidden", no_argument, NULL, 'H' },
		{ NULL, no_argument, NULL, 0 }
	};
	int
		offset = DEFAULT_OFFSET,
		entries = DEFAULT_ENTRIES;
	int opt = -1, hidden = 1;
	char record_delimiter = '\n', only_type = TYPE_ALL;
	dirhead_options* opts;

	while ((opt = getopt_long(argc, argv, shortops, longops, NULL)) != -1) {
		switch(opt) {
			case 'h':
				usage(stdout);
				break;
			case 'o':
				offset = atoi(optarg);
				break;
			case 'n':
				entries = atoi(optarg);
				break;
			case 'V':
				version(stderr);
				break;
			case 't':
				if (!optarg) {
					break;
				}
				only_type = optarg[0];
				switch(only_type) {
					case 'f':
					case 'd':
					case 'c':
					case 'b':
					case 'p':
					case 'l':
					case 's':
						break;
					default:
						fprintf(stderr,
							"%s: not a valid type\n",
							optarg
						);
						exit(EXIT_FAILURE);
						break;
				}
				break;
			case 'd':
				record_delimiter = optarg[0];
				break;
			case '0':
				record_delimiter = '\0';
				break;
			case 'H':
				hidden = 0;
				break;
			default:
				if (optarg != NULL) {
					fprintf(stderr, "unhandled option %s\n", optarg);
				}
				break;
		}
	}
	opts = (dirhead_options*)malloc(sizeof(dirhead_options));

	opts->offset = offset;
	opts->entries = entries;
	opts->ftype = only_type;
	opts->terminator = record_delimiter;
	opts->hidden = hidden;

	return opts;
}

void usage(FILE* stream) {
	fprintf(stream,
		"%s: [OPTIONS] dir1 <dirs...>\n",
		APPNAME
	);
	// TODO: generate this more nicely
	fprintf(stream,
		"  -o, --offset N\t\tskip N files before printing any file names\n"
		"  -n, --entries N\t\temit no more than N file names instead of 100\n"

		"  -d, --delimiter CHAR\t\tprint CHAR after each file name instead of \n"
		"\t\t\t\tnewline. Overrides -0 if specified after it\n"

		"  -0, --null-terminate\t\tafter each file name print null instead of\n"
		"\t\t\t\tnewline\n"
		"\t\t\t\toverrides -d if specified after it\n"

		"  -t, --type TYPE\t\tonly print files of TYPE. One of: fdlcbps \n"

		"  -H, --no-hidden\t\tdo not print files starting with .\n"
	);
	exit(EXIT_FAILURE);
}

void version(FILE* stream) {
	fprintf(stream,
		"%s version %s\n",
		APPNAME, DIRHEAD_VERSION
	);
	exit(EXIT_FAILURE);
}

void dir_head(char* dirname, dirhead_options* opts) {
	DIR* dir;
	struct dirent* dentry;
	int pos = 0, c = 0, rc = -1;

	dir = opendir(dirname);
	if (dir == NULL) {
		perror(dirname);
		return;
	}

	while ((dentry = readdir(dir)) != NULL) {
		if (errno != 0) {
			perror(dirname);
			break;
		}
		if (pos++ < opts->offset) {
			continue;
		}
		if (c++ < opts->entries) {
			if (
				(
					opts->ftype != TYPE_ALL
					&&
					wanted_type(dentry, opts, dirname)
				) != 0
				||
				wanted_file(dentry, opts) == 0
			) {
				// doesn't count toward the total emitted
				c--;
			} else {
				write_dentry(dentry, opts);
			}
		}

		if (c > opts->entries) {
			break;
		}

		errno = 0;
	}

	rc = closedir(dir);

	if (rc == -1) {
		perror(dirname);
	}
}

int wanted_type(struct dirent* dentry, dirhead_options* opts, char* basedir) {
	if (opts->ftype == TYPE_ALL) {
		return 0;
	}

	struct stat info;
	int rc = -1, size;
	char *path;

	size = strlen(basedir) + strlen(dentry->d_name) + 2;
	path = malloc(size);
	if (snprintf(path, size, "%s/%s", basedir, dentry->d_name) < 0) {
		perror(dentry->d_name);
		rc = -1;
		goto DONE;
	}

	if (lstat(path, &info) == -1) {
		perror(path);
		rc = -1;
		goto DONE;
	}

	switch(opts->ftype) {
		case TYPE_ALL:
			rc = 0;
			break;
		case 'f':
			if (S_ISREG(info.st_mode)) {
				rc = 0;
			}
			break;
		case 'd':
			if (S_ISDIR(info.st_mode)) {
				rc = 0;
			}
			break;
		case 'c':
			if (S_ISCHR(info.st_mode)) {
				rc = 0;
			}
			break;
		case 'b':
			if (S_ISBLK(info.st_mode)) {
				rc = 0;
			}
			break;
		case 'p':
			if (S_ISFIFO(info.st_mode)) {
				rc = 0;
			}
			break;
		case 'l':
			if (S_ISLNK(info.st_mode)) {
				rc = 0;
			}
			break;
		case 's':
			if (S_ISSOCK(info.st_mode)) {
				rc = 0;
			}
			break;
		default:
			// nothing, which means skip everything
			break;
	}

	DONE:
	free(path);

	return rc;
}

int wanted_file(struct dirent* dentry, dirhead_options* opts) {
	if (!opts->hidden && dentry->d_name[0] == '.') {
		// do not show hidden files unless requested
		return  0;
	} else if (strcmp(dentry->d_name, ".") == 0 || strcmp(dentry->d_name, "..") == 0) {
		// in any case do not show special dirs
		return 0;
	}
	return 1;
}

void write_dentry(struct dirent* dentry, dirhead_options* opts) {
	printf("%s%c", dentry->d_name, opts->terminator);
}
