#ifndef _DIREAD_H

#define _DIRHEAD_H 1
#define APPNAME "dirhead"
#define DIRHEAD_VERSION "0.1"
#define DEFAULT_OFFSET 0
#define DEFAULT_ENTRIES 100
#define TYPE_ALL '-'

#define _(string) gettext(string)

typedef struct _dirhead_opts {
	int offset;
	int entries;
	int hidden;
	char terminator;
	char ftype;
	char* path;
} dirhead_options;

void usage(FILE* stream);
void version(FILE* stream);
void dir_head(char* dirname, dirhead_options* opts);
void write_dentry(struct dirent* dentry, dirhead_options* opts);

int bad_arg(char* arg);
dirhead_options* mkopts(int argc, char** argv);
int wanted_type(struct dirent* dentry, dirhead_options* opts, char* basedir);
int wanted_file(struct dirent* dentry, dirhead_options* opts);

#endif
